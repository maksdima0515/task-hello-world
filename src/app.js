const express = require("express");
const app = express();
const port = 5500;

app.use((req, res, next) => {
    res.header('Access-Control-Allow-Origin', '*');
    next();
})

app.get("/api/hello", function(req, res) {
    res.send("Hello World - чутка заезженная тема. Чёткие пацаны пишут - ВАСАП ГАЙС");
})

app.listen(port);