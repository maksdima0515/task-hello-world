function sendRequest() {
    fetch("/api/hello")
    .then(response => {
        return response.text();
    })
    .then(data => {
        document.getElementById("test_text").textContent = data;
    })
    .catch(error => {
        document.getElementById("test_text").textContent = `Ошибка: ${error}`;
    })
}